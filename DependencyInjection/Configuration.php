<?php

namespace Ibw\Bundle\GoogleAnalyticsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ibw_google_analytics');

        $rootNode
                ->children()
                ->scalarNode('client_email')
                ->cannotBeEmpty()
                ->isRequired()
                ->end()
                ->scalarNode('private_key')
                ->cannotBeEmpty()
                ->isRequired()
                ->end()
                ->scalarNode('view_id')
                ->cannotBeEmpty()
                ->isRequired()
                ->end()
                ->end();

        return $treeBuilder;
    }

}