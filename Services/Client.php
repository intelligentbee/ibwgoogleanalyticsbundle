<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ibw\Bundle\GoogleAnalyticsBundle\Services;

class Client
{

    private $clientEmail;
    private $privateKey;
    private $client;

    public function __construct($configs)
    {

        $this->clientEmail = $configs['client_email'];
        $this->privateKey = $configs['private_key'];

        $this->client = new \Google_Client();

        $privateKey = file_get_contents($this->privateKey);

        $this->client->setAssertionCredentials(new \Google_Auth_AssertionCredentials(
                $this->clientEmail, array(
            'https://www.googleapis.com/auth/analytics.readonly'
                ), $privateKey)
        );
    }

    public function getGoogleClient()
    {
        return $this->client;
    }

}