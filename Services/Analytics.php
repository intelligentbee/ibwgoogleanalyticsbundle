<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ibw\Bundle\GoogleAnalyticsBundle\Services;

class Analytics extends \Google_Service_Analytics
{

    /**
     * @var Client client
     */
    public $client;

    /**
     * Constructor
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        parent::__construct($client->getGoogleClient());
    }

}