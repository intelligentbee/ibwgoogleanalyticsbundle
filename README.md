# README #

### To install it: ###

```
#!json

"require": { 
    "ibw/google-analytics-bundle": "0.1.*@dev"
}
```

### Then update AppKernel.php ###

...
new Ibw\Bundle\GoogleAnalyticsBundle\IbwGoogleAnalyticsBundle(),
...

### Add this to config.php and replace XXX with your data. ###
```
#!yaml
ibw_google_analytics:
    client_email: "XXXX@developer.gserviceaccount.com"
    private_key: "%kernel.root_dir%/Resources/security/XXX.p12"
    view_id: "ga:XXX"
```